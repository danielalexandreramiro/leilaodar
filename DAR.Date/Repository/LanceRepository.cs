﻿using Microsoft.EntityFrameworkCore;
using DAR.Business.Interfaces.Repository;
using DAR.Business.Models;
using DAR.Date.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAR.Date.Repository
{
    public class LanceRepository : Repository<Lance>, ILanceRepository
    {
        public LanceRepository(MeuDbContext context) : base(context) { }


        public async Task<IEnumerable<Lance>> ObterTodosComInclude()
        {
            return await Db.Lances.AsNoTracking()
                .Include(x=>x.Produto)
                .Include(x=>x.Pessoa)
            .OrderBy(p => p.ValorLance).ToListAsync();
        }

        public async Task<Lance> ObterComInclude(int id)
        {
            var aux = await Db.Lances.AsNoTracking().AsNoTracking()
                .Include(x => x.Produto)
                .Include(x => x.Pessoa)
                .FirstOrDefaultAsync(p => p.Id == id);
            return aux;
        }

        public async Task<Lance> ObterComIncludeProdutoId(int idProduto)
        {
            var aux = await Db.Lances.AsNoTracking().AsNoTracking()
                .Include(x => x.Produto)
                .Include(x => x.Pessoa)
                .FirstOrDefaultAsync(p => p.ProdutoId == idProduto);
            return aux;
        }

        public async Task<List<Lance>> ObterTodosComIncludeProdutoId(int idProduto)
        {
            return await Db.Lances.AsNoTracking().Where(c=>c.ProdutoId== idProduto)
                .Include(x => x.Produto)
                .Include(x => x.Pessoa)
            .OrderBy(p => p.ValorLance).ToListAsync();
        }

        public async Task<decimal> ObterUltimoLanceProdutoId(int idProduto)
        {
            List<Lance> aux = await Db.Lances.AsNoTracking().Where(p => p.ProdutoId == idProduto).ToListAsync();
            decimal valor = 0;

            if (aux.Count == 0)
            {
                var produto = await Db.Produtos.AsNoTracking().FirstAsync(p => p.Id == idProduto);
                valor = produto.Valor;
            }
            else
            {
                valor= aux.Max(p => p.ValorLance);
            }
                          
            return valor;
        }

    }
}
