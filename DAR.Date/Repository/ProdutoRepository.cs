﻿using Microsoft.EntityFrameworkCore;
using DAR.Business.Interfaces.Repository;
using DAR.Business.Models;
using DAR.Date.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAR.Date.Repository
{
    public class ProdutoRepository : Repository<Produto>, IProdutoRepository
    {
        public ProdutoRepository(MeuDbContext context) : base(context) { }


        public async Task<IEnumerable<Produto>> ObterTodosComInclude()
        {
            return await Db.Produtos.AsNoTracking()
            .OrderBy(p => p.Nome).ToListAsync();

        }
       
        public async Task<Produto> ObterComInclude(int id)
        {
            var aux = await Db.Produtos.AsNoTracking().AsNoTracking()
            .FirstOrDefaultAsync(p => p.Id == id);
            return aux;
        }

        public async Task<IEnumerable<Produto>> ObterListaCombobox()
        {
            var lista = await Db.Produtos.AsNoTracking()
                .OrderBy(p => p.Nome).ToListAsync();

            Produto produto = new Produto();
            produto.Id = 0;
            produto.Nome = "---- Selecione um Produto ---- ";
            lista.Insert(0, produto);
            return lista;
        }

    }
}
