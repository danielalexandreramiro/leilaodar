﻿using Microsoft.EntityFrameworkCore;
using DAR.Business.Interfaces.Repository;
using DAR.Business.Models;
using DAR.Date.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAR.Date.Repository
{
    public class PessoaRepository : Repository<Pessoa>, IPessoaRepository
    {
        public PessoaRepository(MeuDbContext context) : base(context) { }


        public async Task<IEnumerable<Pessoa>> ObterTodosComInclude()
        {
            return await Db.Pessoas.AsNoTracking()
            .OrderBy(p => p.Nome).ToListAsync();

        }

        public async Task<Pessoa> ObterComInclude(int id)
        {
            var aux = await Db.Pessoas.AsNoTracking().AsNoTracking()
            .FirstOrDefaultAsync(p => p.Id == id);
            return aux;
        }

        public async Task<IEnumerable<Pessoa>> ObterListaCombobox()
        {
            var lista = await Db.Pessoas.AsNoTracking()
                .OrderBy(p => p.Nome).ToListAsync();

            Pessoa aux = new Pessoa();
            aux.Id = 0;
            aux.Nome = "---- Selecione um Pessoa ---- ";
            lista.Insert(0, aux);
            return lista;
        }

    }
}
