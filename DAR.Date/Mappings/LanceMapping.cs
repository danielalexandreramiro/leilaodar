﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DAR.Business.Models;

namespace DAR.Date.Mappings
{
    public class LanceMapping : IEntityTypeConfiguration<Lance>
    {
        public void Configure(EntityTypeBuilder<Lance> builder)
        {
            builder.HasKey(p => p.Id);

            builder.ToTable("Lances");
        }
    }
}