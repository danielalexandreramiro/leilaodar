﻿using AutoMapper;
using DAR.Business.Models;
using DAR.Web.ViewModels;


namespace DAR.Web.AutoMapper
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {

            CreateMap<Pessoa, PessoaViewModel>().ReverseMap();
            CreateMap<Produto, ProdutoViewModel>().ReverseMap();
            CreateMap<Lance, LanceViewModel>().ReverseMap();
        

        }
    }
}
