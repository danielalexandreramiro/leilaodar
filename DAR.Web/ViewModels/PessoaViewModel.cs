﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DAR.Web.ViewModels
{
    public class PessoaViewModel
    {
        [Display(Name = "Código Pessoa")]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [Display(Name = "Nome Pessoa")]
        public string Nome { get; set; }

        [Display(Name = "Idade Pessoa")]
        [Range(1, 100, ErrorMessage = "O valor para {0} deve estar entre {1} e {2}.")]
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public int Idade { get; set; }
    }
}
