﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DAR.Web.ViewModels
{
    public class ProdutoViewModel
    {
        [Display(Name = "Código Produto")]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [Display(Name = "Nome Produto")]
        public string Nome { get; set; }


        [Display(Name = "Valor Produto")]
        [Range(0.1, 999999.99, ErrorMessage = "O valor para {0} deve estar entre {1} e {2}.")]
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public decimal Valor { get; set; }
    }
}
