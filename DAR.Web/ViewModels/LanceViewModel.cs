﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DAR.Web.ViewModels
{
    public class LanceViewModel
    {
        [Display(Name = "Código Lance")]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [Display(Name = "Produto")]
        public int ProdutoId { get; set; }

        public ProdutoViewModel Produto { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [Display(Name = "Pessoa")]
        public int PessoaId { get; set; }

        public PessoaViewModel Pessoa { get; set; }

        [Display(Name = "Valor Lance")]
        [Range(0.1, 999999.99, ErrorMessage = "O valor para {0} deve estar entre {1} e {2}.")]
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public decimal ValorLance { get; set; }
    }
}
