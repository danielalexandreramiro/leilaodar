﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DAR.Business.Models;
using DAR.Business.Interfaces.Repository;
using AutoMapper;
using DAR.Web.ViewModels;
using DAR.Business.Interfaces;
using DAR.Business.Interfaces.Services;

namespace DAR.Web.Controllers
{
    public class ProdutosController : BaseController
    {
        private readonly IProdutoRepository _produtoRepository;
        private readonly IProdutoService _produtoService;
        private readonly IMapper _mapper;


        public ProdutosController(IProdutoRepository produtoRepository,
                                 IProdutoService produtoService,
                                 IMapper mapper,
                                 INotificador notificador) : base(notificador)
        {
            _produtoRepository = produtoRepository;
            _produtoService = produtoService;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            return View(_mapper.Map<IEnumerable<ProdutoViewModel>>(await _produtoRepository.ObterTodosComInclude()));

        }

        public async Task<IActionResult> Details(int id)
        {
            var viewModel = _mapper.Map<ProdutoViewModel>(await _produtoRepository.ObterPorId(id));
            if (viewModel == null)
            {
                return NotFound();
            }

            return View(viewModel);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProdutoViewModel viewModel)
        {
            if (ModelState.IsValid)
            {

                var entidade = _mapper.Map<Produto>(viewModel);

                await _produtoService.Adicionar(entidade);

                if (!OperacaoValida()) return View(viewModel);

                return RedirectToAction(nameof(Index));
            }
            return View(viewModel);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var viewModel = _mapper.Map<ProdutoViewModel>(await _produtoRepository.ObterPorId(id));
            if (viewModel == null)
            {
                return NotFound();
            }
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ProdutoViewModel viewModel)
        {
            if (id != viewModel.Id) return NotFound();

            if (!ModelState.IsValid) return View(viewModel);

            var entidade = _mapper.Map<Produto>(viewModel);

            await _produtoService.Atualizar(entidade);

            if (!OperacaoValida()) return View(viewModel);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Delete(int id)
        {
            var viewModel = _mapper.Map<ProdutoViewModel>(await _produtoRepository.ObterPorId(id));
            if (viewModel == null)
            {
                return NotFound();
            }
            return View(viewModel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _produtoService.Remover(id);
            TempData["Sucesso"] = "Produto excluido com sucesso!";
            return RedirectToAction("Index");
        }

    }
}
