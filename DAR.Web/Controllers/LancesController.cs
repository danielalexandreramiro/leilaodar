﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DAR.Business.Models;
using DAR.Business.Interfaces.Repository;
using AutoMapper;
using DAR.Web.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAR.Business.Interfaces;
using DAR.Business.Interfaces.Services;
using System;

namespace DAR.Web.Controllers
{
    public class LancesController : BaseController
    {
        private readonly ILanceRepository _lanceRepository;
        private readonly ILanceService _lanceService;
        private readonly IProdutoRepository _produtoRepository;
        private readonly IPessoaRepository _pessoaRepository;
        private readonly IMapper _mapper;


        public LancesController(ILanceRepository lanceRepository,
                                 ILanceService lanceService,
                                 IProdutoRepository produtoRepository,
                                 IPessoaRepository  pessoaRepository,
                                 IMapper mapper,
                                 INotificador notificador) : base(notificador)
        {
            _lanceService = lanceService;
            _lanceRepository = lanceRepository;
            _produtoRepository = produtoRepository;
            _pessoaRepository = pessoaRepository;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            return View(_mapper.Map<IEnumerable<LanceViewModel>>(await _lanceRepository.ObterTodosComInclude()));

        }
        public async Task<IActionResult> BuscaRapida(int id)
        {
            var lances = _mapper.Map<IEnumerable<LanceViewModel>>(await _lanceRepository.ObterTodosComIncludeProdutoId(id));
            return View("Index",lances);
        }

        public async Task<IActionResult> Details(int id)
        {
            var viewModel = _mapper.Map<LanceViewModel>(await _lanceRepository.ObterComInclude(id));
            if (viewModel == null)
            {
                return NotFound();
            }

            return View(viewModel);
        }

        public async Task<IActionResult> Create()
        {
            ViewData["PessoaId"] = new SelectList(await _pessoaRepository.ObterListaCombobox(), "Id", "Nome");
            ViewData["ProdutoId"] = new SelectList(await _produtoRepository.ObterListaCombobox(), "Id", "Nome");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(LanceViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var pessoa = _mapper.Map<PessoaViewModel>(await _pessoaRepository.ObterPorId(viewModel.PessoaId));
                var produto = _mapper.Map<ProdutoViewModel>(await _produtoRepository.ObterPorId(viewModel.ProdutoId));
                decimal maxlance = 0;

                if (viewModel.PessoaId != 0 && viewModel.ProdutoId != 0)
                {                    
                    if (pessoa.Idade < 18)
                    {
                        TempData["errolance"] = "A pessoa precisa ter mais de 18 anos para realizar lances";
                    }
                    else
                    {
                        List<LanceViewModel> atual = _mapper.Map<List<LanceViewModel>>(await _lanceRepository.ObterTodosComIncludeProdutoId(viewModel.ProdutoId));

                        if (atual.Count > 0)
                            maxlance = maiorValor(atual);
                        
                        if (atual.Count == 0 && viewModel.ValorLance <= produto.Valor)                
                            TempData["errolance"] = "O lance para o produto " + produto.Nome + " precisa ser maior que o valor do produto de R$ " + produto.Valor + " (lance inícial).";                        
                        else if (atual.Count > 0 && viewModel.ValorLance <= maxlance)
                            TempData["errolance"] = "O lance para o produto " + produto.Nome + " precisa ser maior que o lance anterior no valor atual R$ " + maxlance;
                        else 
                        { 
                            var entidade = _mapper.Map<Lance>(viewModel);

                            await _lanceService.Adicionar(entidade);

                            if (!OperacaoValida()) return View(viewModel);

                            TempData["lanceok"] = pessoa.Nome + " seu lance foi aceito no valor de R$ " + viewModel.ValorLance + " para o produto " + produto.Nome;

                            return RedirectToAction(nameof(Index));
                        }
                    }
                }
                else
                {
                    TempData["errocombo"] = "Escolha um produto e uma pessoa para confirmar o lance";
                }
            }
            ViewData["PessoaId"] = new SelectList(await _pessoaRepository.ObterListaCombobox(), "Id", "Nome");
            ViewData["ProdutoId"] = new SelectList(await _produtoRepository.ObterListaCombobox(), "Id", "Nome");
            return View(viewModel);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var viewModel = _mapper.Map<LanceViewModel>(await _lanceRepository.ObterComInclude(id));
            if (viewModel == null)
            {
                return NotFound();
            }
            ViewData["PessoaId"] = new SelectList(await _pessoaRepository.ObterListaCombobox(), "Id", "Nome");
            ViewData["ProdutoId"] = new SelectList(await _produtoRepository.ObterListaCombobox(), "Id", "Nome");
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, LanceViewModel viewModel)
        {
            if (id != viewModel.Id) return NotFound();

            if (!ModelState.IsValid) return View(viewModel);

            var entidade = _mapper.Map<Lance>(viewModel);

            await _lanceRepository.Atualizar(entidade);

            if (!OperacaoValida()) return View(viewModel);

            ViewData["PessoaId"] = new SelectList(await _pessoaRepository.ObterTodos(), "Id", "Nome");
            ViewData["ProdutoId"] = new SelectList(await _produtoRepository.ObterTodos(), "Id", "Nome");
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Delete(int id)
        {
            var viewModel = _mapper.Map<LanceViewModel>(await _lanceRepository.ObterComInclude(id));
            if (viewModel == null)
            {
                return NotFound();
            }
            return View(viewModel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _lanceService.Remover(id);
            TempData["Sucesso"] = "Lance excluido com sucesso!";
            return RedirectToAction("Index");
        }
        
        public async Task<decimal> LanceAtual(int id)
        {
            return await _lanceRepository.ObterUltimoLanceProdutoId(id);
        }

        private decimal maiorValor(List<LanceViewModel> lista)
        {
            decimal valor = 0;
            foreach(var item in lista)
                if(item.ValorLance > valor)
                    valor = item.ValorLance;
            
            return valor;
        }

    }
}
