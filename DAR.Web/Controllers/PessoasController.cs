﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DAR.Business.Models;
using DAR.Business.Interfaces.Repository;
using AutoMapper;
using DAR.Web.ViewModels;
using DAR.Business.Interfaces;
using DAR.Business.Interfaces.Services;

namespace DAR.Web.Controllers
{
    public class PessoasController : BaseController
    {
        private readonly IPessoaRepository _pessoaRepository;
        private readonly IPessoaService _pessoaService;
        private readonly IMapper _mapper;


        public PessoasController(IPessoaRepository pessoaRepository,
                                 IPessoaService pessoaService,
                                 IMapper mapper,
                                 INotificador notificador) : base(notificador)
        {
            _pessoaRepository = pessoaRepository;
            _pessoaService = pessoaService;
            _mapper = mapper;
        }
      
        public async Task<IActionResult> Index()
        {
            return View(_mapper.Map<IEnumerable<PessoaViewModel>>(await _pessoaRepository.ObterTodosComInclude()));
            
        }

        public async Task<IActionResult> Details(int id)
        {
            var viewModel = _mapper.Map<PessoaViewModel>(await _pessoaRepository.ObterPorId(id));
            if (viewModel == null)
            {
                return NotFound();
            }

            return View(viewModel);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PessoaViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.Id = 0;

                var entidade = _mapper.Map<Pessoa>(viewModel);

                await _pessoaService.Adicionar(entidade);

                if (!OperacaoValida()) return View(viewModel);

                return RedirectToAction(nameof(Index));
            }
            return View(viewModel);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var viewModel = _mapper.Map<PessoaViewModel>(await _pessoaRepository.ObterPorId(id));
            if (viewModel == null)
            {
                return NotFound();
            }
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, PessoaViewModel viewModel)
        {
            if (id != viewModel.Id) return NotFound();

            if (!ModelState.IsValid) return View(viewModel);

            var entidade = _mapper.Map<Pessoa>(viewModel);

            await _pessoaService.Atualizar(entidade);

            if (!OperacaoValida()) return View(viewModel);

            return RedirectToAction("Index");           
        }

        public async Task<IActionResult> Delete(int id)
        {
            var viewModel = _mapper.Map<PessoaViewModel>(await _pessoaRepository.ObterPorId(id));
            if (viewModel == null)
            {
                return NotFound();
            }
            return View(viewModel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _pessoaService.Remover(id);
            TempData["Sucesso"] = "Pessoa excluido com sucesso!";
            return RedirectToAction("Index");
        }

    }
}
