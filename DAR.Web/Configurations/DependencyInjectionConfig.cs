﻿using Microsoft.Extensions.DependencyInjection;
using DAR.Business;
using DAR.Business.Interfaces.Repository;
using DAR.Business.Interfaces.Services;
using DAR.Business.Notificacoes;
using DAR.Business.Services;
using DAR.Date.Context;
using DAR.Date.Repository;
using DAR.Business.Interfaces;

namespace DAR.Web.Configurations
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            services.AddScoped<MeuDbContext>();

            services.AddScoped<INotificador, Notificador>();

            services.AddScoped<ILanceService, LanceService>();
            services.AddScoped<IPessoaService, PessoaService>();
            services.AddScoped<IProdutoService, ProdutoService>();

            services.AddScoped<IPessoaRepository, PessoaRepository>();
            services.AddScoped<IProdutoRepository, ProdutoRepository>();
            services.AddScoped<ILanceRepository, LanceRepository>();
           

            return services;
        }
    }
}
