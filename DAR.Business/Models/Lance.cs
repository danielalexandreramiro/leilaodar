﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAR.Business.Models
{
    public class Lance : Entity
    {

        public int ProdutoId { get; set; }

        public Produto Produto { get; set; }

        public int PessoaId { get; set; }

        public Pessoa Pessoa { get; set; }

        public decimal ValorLance { get; set; }
    }
}
