﻿using FluentValidation;

namespace DAR.Business.Models.Validations
{
    public class LanceValidation : AbstractValidator<Lance>
    {
        public LanceValidation()
        {           
            RuleFor(c => c.ValorLance)
                .GreaterThan(0).WithMessage("O campo {PropertyName} precisa ser maior que {ComparisonValue}");

        }
    }

}
