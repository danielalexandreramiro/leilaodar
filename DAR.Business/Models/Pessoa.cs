﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAR.Business.Models
{
    public class Pessoa : Entity
    {
        public string Nome { get; set; }

        public int Idade { get; set; }
    }
}
