﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAR.Business.Models
{
    public class Produto : Entity
    {
        public string Nome { get; set; }

        public decimal Valor { get; set; }
    }
}
