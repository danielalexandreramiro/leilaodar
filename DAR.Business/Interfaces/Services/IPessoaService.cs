﻿using DAR.Business.Models;
using System;
using System.Threading.Tasks;

namespace DAR.Business.Interfaces.Services
{
    public interface IPessoaService : IDisposable
    {
        Task Adicionar(Pessoa pessoa);
        Task Atualizar(Pessoa pessoa);
        Task Remover(int id);
    }
}
