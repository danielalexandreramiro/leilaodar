﻿using DAR.Business.Models;
using System;
using System.Threading.Tasks;

namespace DAR.Business.Interfaces.Services
{
    public interface ILanceService : IDisposable
    {
        Task Adicionar(Lance lance);
        Task Remover(int id);
    }
}
