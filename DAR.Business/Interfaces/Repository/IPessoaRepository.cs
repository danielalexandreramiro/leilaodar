﻿using DAR.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAR.Business.Interfaces.Repository
{
    public interface IPessoaRepository : IRepository<Pessoa>
    {
        Task<IEnumerable<Pessoa>> ObterTodosComInclude();
        Task<Pessoa> ObterComInclude(int id);
        Task<IEnumerable<Pessoa>> ObterListaCombobox();
    }
}
