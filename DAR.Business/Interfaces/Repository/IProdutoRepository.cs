﻿using DAR.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAR.Business.Interfaces.Repository
{
    public interface IProdutoRepository : IRepository<Produto>
    {
        Task<IEnumerable<Produto>> ObterTodosComInclude();
        Task<Produto> ObterComInclude(int id);
        Task<IEnumerable<Produto>> ObterListaCombobox();

    }
}
