﻿using DAR.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAR.Business.Interfaces.Repository
{
    public interface ILanceRepository : IRepository<Lance>
    {
        Task<IEnumerable<Lance>> ObterTodosComInclude();
        Task<Lance> ObterComInclude(int id);
        Task<Lance> ObterComIncludeProdutoId(int idProduto);
        Task<List<Lance>> ObterTodosComIncludeProdutoId(int idProduto);
        Task<decimal> ObterUltimoLanceProdutoId(int idProduto);
    }
}
