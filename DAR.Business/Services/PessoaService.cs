﻿using DAR.Business.Interfaces;
using DAR.Business.Interfaces.Repository;
using DAR.Business.Interfaces.Services;
using DAR.Business.Models;
using DAR.Business.Models.Validations;
using System.Threading.Tasks;

namespace DAR.Business.Services
{
    public class PessoaService : BaseService, IPessoaService
    {
        private readonly IPessoaRepository _pessoaRepository;


        public PessoaService(IPessoaRepository pessoaRepository,
                                  INotificador notificador) : base(notificador)
        {
            _pessoaRepository = pessoaRepository;
        }

        
       public async Task Adicionar(Pessoa pessoa)
       {
            if (!ExecutarValidacao(new PessoaValidation(), pessoa)) return;

            await _pessoaRepository.Adicionar(pessoa);
       }

        public async Task Atualizar(Pessoa pessoa)
        {
            if (!ExecutarValidacao(new PessoaValidation(), pessoa)) return;

            await _pessoaRepository.Atualizar(pessoa);
        }

        public async Task Remover(int id)
        {
            await _pessoaRepository.Remover(id);
        }



        public void Dispose()
        {
            _pessoaRepository?.Dispose();
        }


    }
}
