﻿using DAR.Business.Interfaces;
using DAR.Business.Interfaces.Repository;
using DAR.Business.Interfaces.Services;
using DAR.Business.Models;
using DAR.Business.Models.Validations;
using System.Threading.Tasks;

namespace DAR.Business.Services
{
    public class LanceService : BaseService, ILanceService
    {
        private readonly IPessoaRepository _pessoaRepository;
        private readonly IProdutoRepository _produtoRepository;
        private readonly ILanceRepository _lanceRepository;


        public LanceService(IPessoaRepository pessoaRepository,
                                  IProdutoRepository produtoRepository,
                                  ILanceRepository lanceRepository,
                                  INotificador notificador) : base(notificador)
        {
            _pessoaRepository = pessoaRepository;
            _produtoRepository = produtoRepository;
            _lanceRepository = lanceRepository;
        }

        
       public async Task Adicionar(Lance lance)
        {
            if (!ExecutarValidacao(new LanceValidation(), lance)) return;

            await _lanceRepository.Adicionar(lance);
        }

        public async Task Remover(int id)
        {
            await _lanceRepository.Remover(id);
        }


        public void Dispose()
        {
            _pessoaRepository?.Dispose();
            _produtoRepository?.Dispose();
            _lanceRepository?.Dispose();
        }


    }
}
