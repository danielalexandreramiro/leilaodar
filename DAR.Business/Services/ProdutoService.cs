﻿using DAR.Business.Interfaces;
using DAR.Business.Interfaces.Repository;
using DAR.Business.Interfaces.Services;
using DAR.Business.Models;
using DAR.Business.Models.Validations;
using System.Threading.Tasks;

namespace DAR.Business.Services
{
    public class ProdutoService : BaseService, IProdutoService
    {
        private readonly IProdutoRepository _produtoRepository;


        public ProdutoService(IProdutoRepository produtoRepository,
                                  INotificador notificador) : base(notificador)
        {
            _produtoRepository = produtoRepository;
        }

        
       public async Task Adicionar(Produto produto)
        {
            if (!ExecutarValidacao(new ProdutoValidation(), produto)) return;

            await _produtoRepository.Adicionar(produto);
        }

        public async Task Atualizar(Produto produto)
        {
            if (!ExecutarValidacao(new ProdutoValidation(), produto)) return;

            await _produtoRepository.Atualizar(produto);
        }

        public async Task Remover(int id)
        {
            await _produtoRepository.Remover(id);
        }


        public void Dispose()
        {
            _produtoRepository?.Dispose();
        }


    }
}
